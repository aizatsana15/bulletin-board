import pytest
from django.urls import reverse


@pytest.mark.django_db
def test_checks_api_url_for_bb(client):
    response = client.get(reverse('api_bb'))
    assert response.status_code == 200


class TestChecksUrlBbDetail:
    url_get_detail = 'detail'

    @pytest.fixture
    def url(self, bb):
        return reverse(self.url_get_detail, kwargs={'pk': bb.pk})

    @pytest.mark.django_db
    def test_checks_api_url_for_bb_detail(self, client, url):
        response = client.get(url)
        assert response.status_code == 200
