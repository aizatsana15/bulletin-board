import logging

from django.conf import settings
from django.core.mail import mail_managers, send_mail
from django.template.loader import render_to_string
from django.core.signing import Signer
from datetime import datetime

logger = logging.getLogger('django.requests')

signer = Signer()


def send_activation_notification(user):
    context = {
        'user': user,
        'host': f'{settings.PROTOCOL}{settings.HOST}',
        'sign': signer.sign(user.username)
    }
    subject = render_to_string('email/activation_letter_subject.txt', context)
    body_text = render_to_string('email/activation_letter_body.txt', context)
    status = send_mail(subject, body_text, settings.EMAIL_HOST_USER, [user.email])
    logger.info(f'Activation notification to user with user_id = {user.pk} was sent with status = {status}')


def get_timestamp_path(instance, filename):
    return '{datetime}_{file_name}'.format(datetime=datetime.now().strftime("%Y_%m_%d_%H_%M_%S"), file_name=filename)


def send_new_comment_notification(comment, recipient='manager'):
    author = comment.bb.author
    context = {
        'author': author,
        'host': f'{settings.PROTOCOL}{settings.HOST}',
        'comment': comment
    }
    subject = render_to_string('email/new_comment_letter_subject.txt', context)
    if recipient == 'client':
        body_text = render_to_string('email/new_comment_letter_body.txt', context)
        status = send_mail(subject, body_text, settings.EMAIL_HOST_USER, [author.email])
        logger.info(f'Activation notification to user with user_id = {author.pk} was sent with status = {status}')

    else:
        body_text = render_to_string('email/new_comment_service_notification_body.txt', context)
        mail_managers(subject, body_text)

