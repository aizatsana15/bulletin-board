from django.apps import AppConfig


class MainConfig(AppConfig):
    name = 'main'
    verbose_name = 'Доска объявления'

    def ready(self):
        import main.signals  # noqa
