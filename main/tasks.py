from __future__ import absolute_import, unicode_literals
from django.core.mail import send_mail


from bboard import celery_app


@celery_app.task
def send_email_task():
    send_mail(
        'Celery Task Worked',
        'This is proof the task worked',
        'hitechnic7670850@gmail.com',
        ['aizatsana15@gmail.com'],

        )
    return None
