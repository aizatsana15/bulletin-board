from django.dispatch import Signal, receiver
from django.db.models.signals import post_save

import logging

from main.utilities import send_activation_notification, send_new_comment_notification
from .models import Comment

logger = logging.getLogger('django.requests')
user_registrated = Signal(providing_args=['instance'])


def user_registrated_dispatcher(sender, **kwargs):
    """
    This function send letter on email user for activation registration
    """
    logger.info(f'Prepare to send activation notification to user with user_id = {kwargs["instance"].pk}')
    send_activation_notification(kwargs['instance'])


user_registrated.connect(user_registrated_dispatcher)


@receiver(post_save, sender=Comment)
def save_comment(sender, instance, **kwargs):
    """
    This function send message to manager when new comment was added,

    if comment status is active and author want to receive notifications,
    function send massage to author about new comment
    """
    author = instance.bb.author
    if not kwargs['created'] and author.send_messages and instance.is_active:
        logger.info(
            f'Prepare to send new comment notification with comment_id = {instance.pk} to user with '
            f'user_id = {author.pk}'
        )
        send_new_comment_notification(instance, recipient='client')
    else:
        logger.info(f'Prepare to send new comment notification to managers for review')
        send_new_comment_notification(instance)
