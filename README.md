## Сontents
* [Name](#name)
* [General info](#general-info)
* [Setup](#setup)

## Name
Bulletin Board
## General info

Bboard is a simple site that gives you ability create announcement and manage them.

You can find life site [here](https://bullletboard.herokuapp.com/).

## Setup  
To run this project:
```
$ git clone https://gitlab.com/aizatsana15/bulletin-board.git
$ pip3 install -r requirements.txt
$ python3 manage.py migrate
$ python3 manage.py loaddata fixtures.json  
```

For work with celery:

If you don`t want to install some services (like RabbitMQ, Redis, etc.) you could use Docker. 
For installation instructions refer to the Docker [here](https://www.digitalocean.com/community/tutorials/docker-ubuntu-18-04-1-ru).

1 Install && run RabbitMQ
```
$ docker pull rabbitmq 
$ docker run -it --rm --name rabbitmq -p 5672:5672 -p 15672:15672 rabbitmq:3-management
```

2 Add queue "celery" in RabbitMQ
```
You can do this via web page: http://localhost:15672/  (default login/password: guest)
```

3 Install && run Redis
```
$ docker pull redis
$ docker run --name bboard-redis redis
```

4 Start Celery
```
$ celery -A bboard worker -l info
```

5 Monitoring Celery tasks execution:
```
$ flower --port=5555
And you can access web page via address: http://localhost:5555
```

If you want to send messages, you need to configure authenticated credentials for the SMTP-server.

