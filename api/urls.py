from django.urls import path

from .views import bbs, BbDetailView, comments

urlpatterns = [
    path('bbs/<int:pk>/comments/', comments),
    path('bbs/<int:pk>/', BbDetailView.as_view(), name='detail'),
    path('bbs/', bbs, name='api_bb'),
]
