import os
from celery import Celery

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'bboard.settings.prod')

app = Celery('bboard')
app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks()
