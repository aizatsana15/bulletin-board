import pytest
from model_bakery import baker

from main.models import AdvUser, Rubric, Bb


@pytest.fixture
def author(db):
    return baker.make(
        AdvUser,
        username='Anastasiia',
        email='asya@gmail.com'
    )


@pytest.fixture
def rubric(db):
    return baker.make(
        Rubric,
        name='Devices'
    )


@pytest.fixture
def bb(db, author, rubric):
    return baker.make(
        Bb,
        title='Pocket book',
        content='in working order',
        price=100,
        contacts=8998,
        author_id=author.id,
        rubric_id=rubric.id
    )
