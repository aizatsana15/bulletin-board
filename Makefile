runserver:
	@$ python3 manage.py runserver --settings=bboard.settings

migrations:
	@$ python3 manage.py makemigrations --settings=bboard.settings

migrate:
	@$ python3 manage.py migrate --settings=bboard.settings

git:
	@$ git add .
	@$ git commit -m "added some changes"
	@$ git push

checkm:
	@$ git checkout master

checkup:
	@$ git checkout update

branch:
	@$ git branch
